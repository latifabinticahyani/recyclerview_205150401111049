package id.filkom.papb.recyclerview_205150401111049;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    EditText et_nim, et_nama;
    Button btn_simpan;
    MahasiswaAdapter mahasiswaAdapter;
    ArrayList<Mahasiswa> mahasiswaList = new ArrayList<>();
    public static String TAG = "RV1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        rv1.setHasFixedSize(true);

        btn_simpan = findViewById(R.id.btn_save);
        et_nim = findViewById(R.id.et_nim);
        et_nama = findViewById(R.id.et_nama);

        btn_simpan.setOnClickListener(v ->{
            mahasiswaList.add(new Mahasiswa(et_nim.getText().toString(), et_nama.getText().toString()));
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        mahasiswaList.add(new Mahasiswa("1851006001110011", "Tirta"));
        mahasiswaList.add(new Mahasiswa("1851006001110012", "Kara"));
        mahasiswaAdapter = new MahasiswaAdapter(this, mahasiswaList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        rv1.setLayoutManager(layoutManager);

        mahasiswaAdapter.setOnItemClickistener((position, v) -> {
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        rv1.setAdapter(mahasiswaAdapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
    }
}